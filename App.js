import React from 'react';
import { Image } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from "react-navigation-tabs";
import { createDrawerNavigator } from 'react-navigation-drawer';
import { Width } from './src/UtilMethods/UtilsMethods';
import Drawer from './src/components/Drawer';
import HomeScreen from './src/screens/HomeScreen';
import Landing from './src/screens/Landing';
import Login from './src/screens/Login';
import Register from './src/screens/Register';
import Store from './src/screens/Store';
import DeliverySchedule from './src/screens/DeliverySchedule';
import Setting from './src/screens/Setting';
import UpdateMarket from './src/screens/UpdateMarket';
import AddCategory from './src/screens/AddCategory';
import AddBrand from './src/screens/AddBrand';
import AddProducts from './src/screens/AddProducts';
import ProductStatus from './src/screens/ProductStatus';
import Reports from './src/screens/Reports';
import Inventory from './src/screens/Inventory';
import InventoryDetail from './src/screens/InventoryDetail';
import CreateProduct from './src/screens/CreateProduct';
import WeeklyReport from './src/screens/WeeklyReport';

console.disableYellowBox = true;

const TabNavigator = createBottomTabNavigator(
    {
        Store: {
            screen: Store,
            navigationOptions: {
                tabBarLabel:"Store",
                tabBarIcon: ({ tintColor }) => (
                    <Image source={require('./src/assets/images/store.png')}
                           style={{
                               width: Width('12%'),
                               height: Width('12%'),
                               tintColor: tintColor}}
                    />
                )
            },
        },
        DeliverySchedule: {
            screen: DeliverySchedule,
            navigationOptions: {
                tabBarLabel:"Store",
                tabBarIcon: ({ tintColor }) => (
                    <Image source={require('./src/assets/images/deliverySchedule.png')}
                           style={{
                               width: Width('12%'),
                               height: Width('12%'),
                               tintColor: tintColor}}
                    />
                )
            },
        },
        Settings: {
            screen: Setting,
            navigationOptions: {
                tabBarLabel:"Store",
                tabBarIcon: ({ tintColor }) => (
                    <Image source={require('./src/assets/images/Settings.png')}
                           style={{
                               width: Width('12%'),
                               height: Width('12%'),
                               tintColor: tintColor}}
                    />
                )
            },
        }
    },
    {
        tabBarOptions: {
            showLabel: false,
            activeTintColor: "#FF6F00",
            inactiveTintColor: "#000000",
            style: {
                // backgroundColor: "#002580",
                height: 70
            }
        }
    }
);

const ProfileNavigator = createDrawerNavigator({
    Drawer: TabNavigator
}, {
    initialRouteName: 'Drawer',
    navigationOptions: { header: null },
    contentComponent: Drawer,
    drawerWidth: 300,
    // drawerBackgroundColor: '#6c9f27'

});

const AppNavigator = createStackNavigator({
    Home: {
        screen: HomeScreen,
    },
    Landing: {
        screen: Landing,
    },
    Login: {
        screen: Login,
    },
    Register: {
        screen: Register,
    },
    TabNavigator: {
        screen: ProfileNavigator,
    },
    UpdateMarket: {
        screen: UpdateMarket,
    },
    AddCategory: {
        screen: AddCategory,
    },
    AddBrand: {
        screen: AddBrand,
    },
    AddProducts: {
        screen: AddProducts,
    },
    CreateProduct: {
        screen: CreateProduct,
    },
    ProductStatus: {
        screen: ProductStatus,
    },
    Reports: {
        screen: Reports,
    },
    Inventory: {
        screen: Inventory,
    },
    InventoryDetail: {
        screen: InventoryDetail,
    },
    WeeklyReport: {
        screen: WeeklyReport,
    },
});

export default createAppContainer(AppNavigator);
