import {FontSize} from './UtilsMethods';

const Fonts = {
    gothamRounded_bold: {
        fontFamily: 'GOTHAMROUNDED-BOLD',
        fontSize: FontSize('14'),
    },
    gothamRounded_light: {
        fontFamily: 'GOTHAMROUNDED-LIGHT',
        fontSize: FontSize('14'),
    },
    gothamRounded_book: {
        fontFamily: 'GOTHAMROUNDEDBOOK',
        fontSize: FontSize('14'),
    }
};

export default Fonts;
