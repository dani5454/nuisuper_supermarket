export default class Singlenton {

    static myInstance = null;

    _user = "";

    /**
     * @returns {Singlenton}
     */
    static getInstance() {
        if (Singlenton.myInstance == null) {
            Singlenton.myInstance = new Singlenton();
        }

        return this.myInstance;
    }

    // Getter Setter of USER

    getMarket() {
        return this._user;
    }

    setMarket(res) {
        this._user = res;
    }
}
