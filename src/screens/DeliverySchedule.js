import React from 'react';
import {
    View,
    Text,
    TouchableWithoutFeedback,
    SafeAreaView,
    StyleSheet,
    ScrollView,
    FlatList,
} from 'react-native';
import Fonts from '../UtilMethods/FontFamily';
import {Height, Width} from '../UtilMethods/UtilsMethods';
import DeliveryCard from '../components/DeliveryCard';
import { NavigationEvents } from 'react-navigation';
import firebaseModal from '../firebase/Firebase';
import Singlenton from '../UtilMethods/Singlenton';

export default class DeliverySchedule extends React.Component {
    common = Singlenton.getInstance();

    constructor(props) {
        super(props);

        this.state = {
            tabNavigator: false,
            onProcess: [],
            completeOrders: [],
        }
    }

    componentDidMount() {
        // firebaseModal.fetchOrders((resp) => {
        //     let onProcess = []; let completeOrders = [];
        //     if(!resp.error) {
        //         resp.data.filter((item) => {
        //             if(item.data().userDetail !== '') {
        //                 onProcess.push(item.data());
        //                 if (item.data().status === 'completed') {
        //                     completeOrders.push(item.data());
        //                 }
        //             }
        //         });
        //         this.setState({completeOrders, onProcess})
        //     }
        // })
    }

    firstTab = () => {
        if(this.state.tabNavigator) {
            this.setState({tabNavigator: false})
        }
    };

    secondTab = () => {
        if(!this.state.tabNavigator) {
            this.setState({tabNavigator: true})
        }
    };

    renderOrders = (item, index) => {
        // if(item.userOrder[0].shop === this.common.getMarket().name) {
        let name = item.userDetail.name + ' ' + item.userDetail.lastName;
        let address = item.userDetail.address[0].addressAndNumber;
        let userImage = item.userDetail.socialProfilePic;
        if (item.status === 'driver_on_way') {
            return <DeliveryCard type={'selection'}
                                 index={item.orderId}
                                 clientName={name}
                                 Direction={address}
                                 image={userImage}
            />
        }
        // }
    };

    renderCompleteOrders = (order, index) => {
        // if(order.userOrder[0].shop === this.common.getMarket().name) {
        let name = order.userDetail.name + ' ' + order.userDetail.lastName;
        let address = order.userDetail.address[0].addressAndNumber;
        let userImage = order.userDetail.socialProfilePic;
        return <DeliveryCard type={'complete'}
                             index={order.orderId}
                             clientName={name}
                             Direction={address}
                             image={userImage}
                             onComplete={this.onComplete.bind(this, order)}
        />
        // }
    };

    onComplete = (order) => {};

    _onWillFocus = (payload) => {
        firebaseModal.fetchOrders((resp) => {
            let onProcess = []; let completeOrders = [];
            if(!resp.error) {
                resp.data.filter((item) => {
                    if(item.data().userDetail !== '') {
                        if (item.data().status === 'completed') {
                            completeOrders.push(item.data());
                        } else if(item.data().status === 'driver_on_way') {
                            onProcess.push(item.data());
                        }
                    }
                });
                this.setState({completeOrders, onProcess})
            }
        })
    };

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <NavigationEvents onWillFocus={payload => this._onWillFocus(payload)} />
                <View style={{justifyContent: 'center', flexDirection: 'row'}}>
                    <TouchableWithoutFeedback
                        onPress={() => this.firstTab()}
                    >
                        <View style={[styles.topLeftNavigator, styles.content, {backgroundColor: !this.state.tabNavigator?'#6c9f27':'#ffffff'}]}>
                            <Text style={Fonts.gothamRounded_bold}>Despachada</Text>
                        </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback
                        onPress={() => this.secondTab()}
                    >
                        <View style={[styles.topRightNavigator, styles.content, {backgroundColor: this.state.tabNavigator?'#6c9f27':'#ffffff'}]}>
                            <Text style={Fonts.gothamRounded_bold}>Completada</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                <View style={styles.container}>
                    <ScrollView>
                        {!this.state.tabNavigator && (
                            <FlatList
                                data={this.state.onProcess}
                                renderItem={({ item, index }) => this.renderOrders(item, index)}
                                keyExtractor={(item, index) => `message ${index}`}
                            />
                        )}
                        {this.state.tabNavigator && (
                            <FlatList
                                data={this.state.completeOrders}
                                renderItem={({ item, index }) => this.renderCompleteOrders(item, index)}
                                keyExtractor={(item, index) => `message ${index}`}
                            />
                        )}
                    </ScrollView>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    topLeftNavigator: {
        borderWidth: 1,
        width: Width('30%'),
        height: Height('3%'),
        borderTopLeftRadius: Width('4%'),
        borderBottomLeftRadius: Width('4%'),
    },
    topRightNavigator: {
        borderWidth: 1,
        width: Width('30%'),
        height: Height('3%'),
        borderTopRightRadius: Width('4%'),
        borderBottomRightRadius: Width('4%'),
    },
    content: {
        alignItems: 'center',
        justifyContent: 'center',
    }
});
