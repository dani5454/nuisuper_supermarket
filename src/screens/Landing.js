import React from 'react';
import {View,ImageBackground,Text,StyleSheet,TouchableOpacity} from 'react-native';
import Fonts from '../UtilMethods/FontFamily';
import { Width, Height } from '../UtilMethods/UtilsMethods';

export default class Landing extends React.Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ImageBackground
                source={require('./../assets/images/landingImg.jpg')}
                style={{ flex: 1 }}>
                <View style={[styles.container, styles.content]}>
                    <View style={styles.btnContainer}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('Register')}
                        >
                            <ImageBackground
                                source={require('./../assets/images/landingBtn.png')}
                                style={[{
                                    width: Width('65%'),
                                    height: Height('7%')

                                }, styles.content]}
                            >
                                <Text style={[Fonts.gothamRounded_bold, {color: '#ffffff'}]}>Crear cuenta</Text>
                            </ImageBackground>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('Login')}
                        >
                            <ImageBackground
                                source={require('./../assets/images/landingBtn.png')}
                                style={[{
                                    width: Width('65%'),
                                    height: Height('7%')
                                }, styles.content]}
                            >
                                <Text style={[Fonts.gothamRounded_bold, {color: '#ffffff'}]}>Iniciar sesión</Text>
                            </ImageBackground>
                        </TouchableOpacity>
                    </View>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    content: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnContainer: {
        height: Height('20%'),
        justifyContent: 'space-between',
    }
});
