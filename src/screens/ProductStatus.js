import React from 'react';
import {View, Text, Image, FlatList, SafeAreaView, StyleSheet, Switch} from 'react-native';
import {Height, Width} from '../UtilMethods/UtilsMethods';
import Singlenton from '../UtilMethods/Singlenton';
import firebaseModal from '../firebase/Firebase';
import Fonts from '../UtilMethods/FontFamily';
import Back from '../components/Back';

export default class ProductStatus extends React.Component {
    common = Singlenton.getInstance();

    static navigationOptions = ({navigation}) => {
        return {
            title: 'Productos',
            headerTitleStyle: Fonts.gothamRounded_bold,
            headerLeft: () => <Back navigation={navigation}/>,
        };
    };

    constructor(props) {
        super(props);

        this.state = {
            productsList: [],
            products: []
        }
    }

    componentDidMount() {
        let productsArr = [];
        firebaseModal.fetchProducts((resp) => {
            if(!resp.error) {
                if(Array.isArray(resp.data.productsList)) {
                    resp.data.productsList.map((item) => {
                        if (Array.isArray(item.products)) {
                            if (Object.entries(item.products[0]).length !== 0) {
                                productsArr.push(...item.products)
                            }
                        }
                    });
                    this.setState({productsList: productsArr,products:resp.data.productsList});
                } else {
                    this.setState({productsList: [],products: []});
                }
            } else {
                alert(resp.errorMessage);
            }
        });
    }

    renderProducts = (item, index) => {
        return (
            <View>
                <View style={styles.productCard}>
                    <View style={{alignItems: 'center'}}>
                        <Image source={{uri: item.image}}
                               style={styles.image}
                               resizeMode={'cover'}
                        />
                    </View>
                    <View style={styles.description}>
                        <Text style={[Fonts.gothamRounded_light,{ marginLeft: 5}]}>{item.name}</Text>
                        <Text style={[Fonts.gothamRounded_light,{ marginLeft: 5}]}>{item.category_type}</Text>
                        <Text style={[Fonts.gothamRounded_bold,{marginLeft: 5}]}>{item.description}</Text>
                        <Text style={[Fonts.gothamRounded_bold,{
                            marginLeft: 5,
                            color: '#6c9f27'
                        }]}>{item.price}{' '}L</Text>
                    </View>
                </View>
                <View style={{flexDirection: 'row',justifyContent: 'center', marginTop: Height('4')}}>
                    <Switch
                        onValueChange = {() => this.toggleSwitch(item, index)}
                        value = {item.active}/>
                </View>
            </View>
        );
    };

    toggleSwitch = (item, index) => {
        const {productsList, products} = this.state;
        productsList[index].active = !productsList[index].active;
        this.setState({productsList});
        firebaseModal.updateMarketStatus(products, (resp) => {
            if(!resp.error) {
                alert('Producto actualizado con éxito');
            } else {
                productsList[index].active = true;
                alert(resp.errorMessage);
            }
        })
    };

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <FlatList
                    data={this.state.productsList}
                    showsVerticalScrollIndicator={false}
                    numColumns={2}
                    columnWrapperStyle={styles.card}
                    renderItem={({item, index}) => this.renderProducts(item, index)}
                    keyExtractor={(item, index) => index.toString()}
                />
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f2efe9'
    },
    card: {
        justifyContent: 'space-around',
        marginTop: Width('6%')
    },
    productCard: {
        backgroundColor: '#FFFFFF',
        width: Width('35%'),
        flexDirection: 'column',
        borderBottomRightRadius: Width('5%'),
        borderBottomLeftRadius: Width('5%')
    },
    image: {
        width: Width('20%'),
        height: Width('20%'),
    },
    description: {
        flexDirection: 'column',
        marginTop: Width('2%')
    }
});
