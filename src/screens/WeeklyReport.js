import React from 'react';
import {View, Text, SafeAreaView, FlatList, StyleSheet, Image} from 'react-native';
import {Height, Width} from '../UtilMethods/UtilsMethods';
import Fonts from '../UtilMethods/FontFamily';
import Back from '../components/Back';
import firebaseModal from '../firebase/Firebase';
import Singlenton from '../UtilMethods/Singlenton';

export default class WeeklyReport extends React.Component {
    common = Singlenton.getInstance();

    static navigationOptions = ({navigation}) => {
        return {
            headerStyle:{
                backgroundColor: '#6c9f27'
            },
            headerLeft: () => <Back navigation={navigation}/>,
            headerTitle: (
                <Image style={{ width: Width('13%'), height: Width('13%') }}
                       source={require('./../assets/images/logo.png')}/>
            ),
        };
    };

    constructor(props) {
        super(props);

        this.state = {
            reports: [],
            total: 0
        }
    }

    componentDidMount() {
        let reports = []; let total = 0;
        firebaseModal.fetchOrders((resp) => {
            if(!resp.error) {
                resp.data.filter((item) => {
                    if(item.data().userDetail !== '') {
                        reports.push(item.data());
                    }
                });
                reports.map((item) => {
                    item.userOrder.forEach((value, index) =>{
                        total += parseInt(value.price) * value.quanity;
                    });
                });
                this.setState({reports,total});
            } else {
                alert(resp.errorMessage);
            }
        });
    }

    renderReports = (item, index) => {
        if(item.userOrder[0].shop === this.common.getMarket().name) {
            let total = 0;
            item.userOrder.forEach((value, index) => {
                total += parseInt(value.price) * value.quanity;
            });
            return (
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                    <Text style={[Fonts.gothamRounded_light, {fontSize: 10}]}>{index}</Text>
                    <Text style={[Fonts.gothamRounded_light, {fontSize: 10}]}>NUI-{item.orderId}</Text>
                    <Text style={[Fonts.gothamRounded_light, {fontSize: 10}]}>{total} L</Text>
                    <Text style={[Fonts.gothamRounded_light, {fontSize: 10}]}>0.00 L</Text>
                    <Text style={[Fonts.gothamRounded_light, {fontSize: 10}]}>0.00 L</Text>
                    <Text style={[Fonts.gothamRounded_light, {fontSize: 10}]}>0.00 L</Text>
                </View>
            );
        }
    };

    renderFooterContent = () => {
        return(
            <View style={{flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={[Fonts.gothamRounded_light, {fontWeight: 'bold',fontSize: 10}]}>Totaies</Text>
                <Text style={[Fonts.gothamRounded_light, {fontWeight: 'bold',fontSize: 10}]}>{this.state.total} L</Text>
                <Text style={[Fonts.gothamRounded_light, {fontWeight: 'bold',fontSize: 10}]}>{''}</Text>
                <Text style={[Fonts.gothamRounded_light, {fontWeight: 'bold',fontSize: 10}]}>0.00 L</Text>
                <Text style={[Fonts.gothamRounded_light, {fontWeight: 'bold',fontSize: 10}]}>0.00 L</Text>
                <Text style={[Fonts.gothamRounded_light, {fontWeight: 'bold',fontSize: 10}]}>0.00 L</Text>
            </View>
        );
    };

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.heading}>
                    <Text style={[Fonts.gothamRounded_bold, {color: 'lightgrey'}]}>Garlic</Text>
                    <Text style={[Fonts.gothamRounded_bold, {color: 'lightgrey'}]}>Garlic - Res. Villas del Campo</Text>
                    <View style={{flex: 1, justifyContent: 'flex-end', alignItems: 'center'}}>
                        <Text style={[Fonts.gothamRounded_bold, {color: 'grey'}]}>Reporte de Ventas</Text>
                        <Text style={[Fonts.gothamRounded_bold, {color: 'grey'}]}>20/09/2019 - 21/09/2019</Text>
                    </View>
                </View>
                <View style={{flex:1, marginTop: 10, marginLeft: 5, marginRight: 5}}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text style={[Fonts.gothamRounded_bold, {fontSize: 10}]}># Fetcha</Text>
                        <Text style={[Fonts.gothamRounded_bold, {fontSize: 10}]}>Orden</Text>
                        <Text style={[Fonts.gothamRounded_bold, {fontSize: 10}]}>Monto</Text>
                        <Text style={[Fonts.gothamRounded_bold, {fontSize: 10}]}>Comisión Hugo</Text>
                        <Text style={[Fonts.gothamRounded_bold, {fontSize: 10}]}>Incidencias</Text>
                        <Text style={[Fonts.gothamRounded_bold, {fontSize: 10}]}>Liquido a pagr</Text>
                    </View>
                    <FlatList
                        data={this.state.reports}
                        bounces={false}
                        scrollEventThrottle={16}
                        style={{ marginTop: Height('2')}}
                        ItemSeparatorComponent={() => <View style={styles.separator} />}
                        showsVerticalScrollIndicator={false}
                        renderItem={({item, index}) => this.renderReports(item, index)}
                        keyExtractor={(item, index) => index.toString()}
                        ListFooterComponent={() => this.renderFooterContent()}
                    />
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    heading: {
        height: Height('12'),
        alignItems: 'center'
    },
    separator: {
        marginTop: 5
    }
});
