import React from 'react';
import {View,Text,TouchableWithoutFeedback,Image,FlatList,SafeAreaView,StyleSheet} from 'react-native';
import {Height, Width} from '../UtilMethods/UtilsMethods';
import Singlenton from '../UtilMethods/Singlenton';
import firebaseModal from '../firebase/Firebase';
import Fonts from '../UtilMethods/FontFamily';
import Back from '../components/Back';

export default class AddProducts extends React.Component {
    common = Singlenton.getInstance();

    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Productos',
            headerTitleStyle: Fonts.gothamRounded_bold,
            headerLeft: () => <Back navigation={navigation} />,
        };
    };

    constructor(props) {
        super(props);

        this.state = {
            productsList: [],
            products: []
        }
    }

    componentDidMount() {
        let productsArr = [];
        firebaseModal.fetchProducts((resp) => {
            if(!resp.error) {
                if(Array.isArray(resp.data.productsList)) {
                    resp.data.productsList.map((item) => {
                        if(Array.isArray(item.products)) {
                            if(Object.entries(item.products[0]).length !== 0) {
                                productsArr.push(...item.products)
                            }
                        }
                    });
                    this.setState({productsList: productsArr,products:resp.data.productsList});
                } else {
                    this.setState({productsList: [],products:[]});
                }
            } else {
                alert(resp.errorMessage);
            }
        });
    }

    onProductsPress = (item) => {
        // const {products} = this.state;
        // console.log('productsList---->>>', products);
        // console.log('Products---->>>', item);
    };

    renderProducts = (item, index) => {
        return (
            <View style={styles.productCard}>
                <TouchableWithoutFeedback
                    style={{flex: 1}}
                    onPress={() => this.onProductsPress(item)}
                >
                    <View style={{flex: 1}}>
                        <View style={{alignItems: 'center'}}>
                            <Image source={{uri: item.image}}
                                   style={styles.image}
                                   resizeMode={'cover'}
                            />
                        </View>
                        <View style={styles.description}>
                            <Text style={[Fonts.gothamRounded_light,{ marginLeft: 5}]}>{item.name}</Text>
                            <Text style={[Fonts.gothamRounded_light,{ marginLeft: 5}]}>{item.category_type}</Text>
                            <Text style={[Fonts.gothamRounded_bold,{marginLeft: 5}]}>{item.description}</Text>
                            <Text style={[Fonts.gothamRounded_bold,{
                                marginLeft: 5,
                                color: '#6c9f27'
                            }]}>{item.price}{' '}L</Text>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    };

    addProduct = () => {
        const {products} = this.state;
        let category = [];
        products.map((item) => {
            category.push(item.marketName);
        });
        this.props.navigation.navigate('CreateProduct', {product: products, category: category});
    };

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <FlatList
                    data={this.state.productsList}
                    style={{height: Height('75')}}
                    showsVerticalScrollIndicator={false}
                    numColumns={2}
                    columnWrapperStyle={styles.card}
                    renderItem={({item, index}) => this.renderProducts(item, index)}
                    keyExtractor={(item, index) => index.toString()}
                />
                <View style={{flex: 1, justifyContent: 'flex-end', alignItems: 'center'}}>
                    <TouchableWithoutFeedback
                        onPress={() => this.addProduct()}
                    >
                        <View style={styles.readyBtn}>
                            <Text style={[Fonts.gothamRounded_bold, {color: '#FFF'}]}>
                                LISTO
                            </Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f2efe9'
    },
    card: {
        justifyContent: 'space-around',
        marginTop: Width('6%'),
    },
    productCard: {
        backgroundColor: '#FFFFFF',
        width: Width('35%'),
        flexDirection: 'column',
        borderBottomRightRadius: Width('5%'),
        borderBottomLeftRadius: Width('5%')
    },
    image: {
        width: Width('20%'),
        height: Width('20%'),
    },
    description: {
        flexDirection: 'column',
        marginTop: Width('2%')
    },
    readyBtn: {
        height: Height('5'),
        width: Width('85'),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: Width('8'),
        backgroundColor: '#6c9f27'
    }
});
