import React from 'react';
import {View,Text,TouchableWithoutFeedback,FlatList,SafeAreaView,StyleSheet} from 'react-native';
import {Height, Width} from '../UtilMethods/UtilsMethods';
import firebaseModal from '../firebase/Firebase';
import Fonts from '../UtilMethods/FontFamily';
import Back from '../components/Back';
import Singlenton from '../UtilMethods/Singlenton';

export default class Inventory extends React.Component {
    common = Singlenton.getInstance();

    static navigationOptions = ({navigation}) => {
        return {
            headerStyle:{backgroundColor: '#6c9f27'},
            headerTitleStyle: Fonts.gothamRounded_bold,
            headerLeft: () => <Back navigation={navigation}/>,
        };
    };

    constructor(props) {
        super(props);

        this.state = {
            inventory: []
        }

    }

    componentDidMount() {
        let inventory = [];
        firebaseModal.fetchOrders((resp) => {
            if(!resp.error) {
                resp.data.filter((item) => {
                    if(item.data().userDetail !== '') {
                        inventory.push(item.data());
                    }
                });
                this.setState({inventory});
            } else {
                alert(resp.errorMessage);
            }
        });
    }

    renderInventory = (item, index) => {
        if(item.userOrder[0].shop === this.common.getMarket().name) {
            return (
                <View style={styles.card}>
                    <View style={{flexDirection: 'row'}}>
                        <Text style={Fonts.gothamRounded_bold}>Desde: </Text>
                        <Text style={Fonts.gothamRounded_light}>{item.orderTime}</Text>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: Width('2')}}>
                        <Text style={Fonts.gothamRounded_bold}>Hasta: </Text>
                        {item.status === 'completed' && (
                            <Text style={Fonts.gothamRounded_light}>Sabado 21 de septiembre de 2020</Text>
                        )}
                    </View>
                    <TouchableWithoutFeedback
                        onPress={() => this.props.navigation.navigate('InventoryDetail', {inventoryInfo: item})}
                    >
                        <View style={styles.button}>
                            <Text style={[Fonts.gothamRounded_bold, {color: '#FFF'}]}>VER ÓRDENES</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            )
        }
    };

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <FlatList
                    data={this.state.inventory}
                    style={{ marginTop: Height('2')}}
                    ItemSeparatorComponent={() => <View style={styles.separator} />}
                    showsVerticalScrollIndicator={false}
                    renderItem={({item, index}) => this.renderInventory(item, index)}
                    keyExtractor={(item, index) => index.toString()}
                    />
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f2efe9',
        alignItems: 'center'
    },
    card: {
        width: Width('95'),
        height: Height('12'),
        backgroundColor: '#FFFFFF',
        borderRadius: Width('2'),
        flexDirection: 'column',
        padding: Width('3')
    },
    button: {
        marginTop: Width('1'),
        height: Height('4'),
        borderRadius: Width('1'),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#839596'
    },
    separator: {
        marginTop: Height('2')
    }
});
