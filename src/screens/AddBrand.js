import React from 'react';
import {View, Text, SafeAreaView, StyleSheet, Image, TextInput, TouchableWithoutFeedback} from 'react-native';
import {Height, Width} from '../UtilMethods/UtilsMethods';
import Fonts from '../UtilMethods/FontFamily';
import Loader from '../components/Loader';
import Back from '../components/Back';
import firebaseModal from '../firebase/Firebase';

export default class AddBrand extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Principle Empresas',
            headerTitleStyle: Fonts.gothamRounded_bold,
            headerRight: () => <Image
                source={require('./../assets/images/logo.png')}
                style={{
                    marginRight: 20,
                    width: Width('10%'),
                    height: Width('10%'),
                }}
            />,
            headerLeft: () => <Back navigation={navigation} />,
        };
    };

    constructor(props) {
        super(props);

        this.state = {
            loader: false,
            brand: '',
            addBrand: false,
        }
    }

    addBrand = () => {
        const {brand} = this.state;
        if(brand.length > 5) {
            this.setState({addBrand: true,loader:true});
            firebaseModal.fetchProducts((resp) => {
                if(!resp.error) {
                    this.updatedProducts(brand, resp.data.brands)
                } else {
                    this.setState({loader: false});
                    setTimeout(() => {
                        alert(resp.errorMessage);
                    }, 500);
                }
            })
        } else {
            alert('Mínimo cinco caracteres requeridos');
        }
    };

    updatedProducts = (br, brands) => {
        let arr = [];
        if(Array.isArray(brands)) {
            brands.push(br);
            this.addBrands(brands);
        } else {
            arr.push(br);
            this.addBrands(arr);
        }
    };

    addBrands = (brands) => {
        firebaseModal.updateMarketBrand(brands, (resp) => {
            if(!resp.error) {
                this.setState({loader: false,brand: '',addBrand: false});
                setTimeout(() => {
                    alert('Negocio agregar con éxito')
                }, 500);
            } else {
                this.setState({loader: false});
                setTimeout(() => {
                    alert(resp.errorMessage);
                }, 500);
            }
        });
    };

    render() {
        return (
            <SafeAreaView style={styles.container}>
                {Loader.renderLoading(this.state.loader)}
                <View style={styles.inputContainer}>
                    <View style={{flexDirection: 'row'}}>
                        <View style={styles.input}>
                            <TextInput
                                style={[Fonts.gothamRounded_bold,{flex: 1}]}
                                autoCapitalize='none'
                                autoCorrect={false}
                                value={this.state.brand}
                                onChangeText={brand => this.setState({brand})}
                            />
                        </View>
                        <TouchableWithoutFeedback
                            onPress={() => this.setState({brand: '',addBrand: false})}
                        >
                            <Image source={require('./../assets/images/cross.png')}
                                   style={styles.arrow}
                                   resizeMode={'cover'}
                            />
                        </TouchableWithoutFeedback>
                    </View>
                </View>
                <View style={styles.categoryContainer}>
                    {this.state.addBrand && (
                        <View style={{alignItems: 'center', marginBottom: Width('5')}}>
                            <Text style={{marginBottom: Width('5')}}>Nombre de Marca</Text>
                            <View style={styles.addCategory}>
                                <Text>{this.state.brand}</Text>
                            </View>
                        </View>
                    )}
                    <TouchableWithoutFeedback
                        onPress={() => this.addBrand()}
                    >
                        <View style={styles.category}>
                            <Text style={[Fonts.gothamRounded_bold, {color: '#ffffff'}]}>AGREGAR EMPRESA</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    inputContainer: {
        borderWidth: 1,
        marginTop: Height('2%'),
        height: Height('4.5%'),
        marginLeft: Width('5%'),
        marginRight: Width('5%'),
        borderRadius: 5,
        borderColor: 'grey',
        justifyContent: 'center'
    },
    circle: {
        width: Width('5%'),
        height: Width('5%'),
        borderRadius: Width('5%')/2,
        marginLeft: Width('4%'),
        backgroundColor: '#D64B12',
    },
    input: {
        width: Width('75%'),
        marginLeft: Width('3%'),
    },
    arrow: {
        height: Width('6%'),
        width: Width('6%')
    },
    categoryContainer: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    addCategory: {
        height:Height('4'),
        width: Width('50'),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#DCDCDC'
    },
    category: {
        backgroundColor: '#6c9f27',
        height: Height('4.5%'),
        marginLeft: Width('4%'),
        marginRight: Width('4%'),
        borderRadius: Width('8%'),
        alignItems: 'center',
        justifyContent: 'center',
    }
});
