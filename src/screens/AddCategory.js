import React from 'react';
import {View, Text, Image,Keyboard, SafeAreaView, StyleSheet, TextInput, TouchableWithoutFeedback} from 'react-native';
import firebaseModal from '../firebase/Firebase';
import Fonts from '../UtilMethods/FontFamily';
import Loader from '../components/Loader';
import Back from '../components/Back';
import {Width, Height} from '../UtilMethods/UtilsMethods';

export default class AddCategory extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Todas las Categorias',
            headerTitleStyle: Fonts.gothamRounded_bold,
            headerRight: () => <Image
                source={require('./../assets/images/logo.png')}
                style={{
                    marginRight: 20,
                    width: Width('10%'),
                    height: Width('10%'),
                }}
            />,
            headerLeft: () => <Back navigation={navigation} />,
        };
    };

    constructor(props) {
        super(props);

        this.state = {
            loader: false,
            category: '',
            addCategory: false,
        }
    }

    add = () => {
        if(this.state.category.length > 5) {
            Keyboard.dismiss();
            this.setState({addCategory: !this.state.addCategory})
        } else {
            alert('Mínimo cinco caracteres requeridos');
        }
    };

    addCategory = () => {
        const {category} = this.state;
        if(category.length > 5) {
            this.setState({loader: true});
            firebaseModal.fetchProducts((resp) => {
                if(!resp.error) {
                    let products = resp.data.productsList;
                    this.updatedProducts(category, products)
                } else {
                    this.setState({loader: false});
                    setTimeout(() => {
                        alert(resp.errorMessage);
                    }, 500);
                }
            })
        }
    };

    updatedProducts = (category, products) => {
        products.push({marketName: category, products: [{}]});
        firebaseModal.updateMarketStatus(products, (resp) => {
            if(!resp.error) {
                this.setState({loader: false});
                setTimeout(() => {
                    alert('Categoría agregada exitosamente');
                }, 500);
            } else {
                this.setState({loader: false});
                setTimeout(() => {
                    alert(resp.errorMessage);
                }, 500);
            }
        });
    };

    render() {
        return (
            <SafeAreaView style={styles.container}>
                {Loader.renderLoading(this.state.loader)}
                <View style={styles.inputContainer}>
                    <View style={{flexDirection: 'row'}}>
                        <View style={styles.circle}/>
                        <View style={styles.input}>
                            <TextInput
                                style={[Fonts.gothamRounded_bold,{flex: 1}]}
                                autoCapitalize='none'
                                autoCorrect={false}
                                value={this.state.category}
                                onEndEditing={() => this.add()}
                                onChangeText={category => this.setState({category})}
                            />
                        </View>
                        <TouchableWithoutFeedback
                            onPress={() => this.add()}
                        >
                            <Image source={require('./../assets/images/forwardArrow.png')}
                                   style={styles.arrow}
                                   resizeMode={'cover'}
                            />
                        </TouchableWithoutFeedback>
                    </View>
                </View>
                <View style={styles.categoryContainer}>
                    {this.state.addCategory && (
                        <View style={{alignItems: 'center', marginBottom: Width('5')}}>
                            <Text style={{marginBottom: Width('5')}}>Nombre de Categoria</Text>
                            <View style={styles.addCategory}>
                                <Text>{this.state.category}</Text>
                            </View>
                        </View>
                    )}
                    <TouchableWithoutFeedback
                        onPress={() => this.addCategory()}
                    >
                        <View style={styles.category}>
                            <Text style={[Fonts.gothamRounded_bold, {color: '#ffffff'}]}>AGREGAR CATEGORIA</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    inputContainer: {
        borderWidth: 1,
        marginTop: Height('2%'),
        height: Height('4.5%'),
        marginLeft: Width('5%'),
        marginRight: Width('5%'),
        borderRadius: 5,
        borderColor: 'grey',
        justifyContent: 'center'
    },
    circle: {
        width: Width('5%'),
        height: Width('5%'),
        borderRadius: Width('5%')/2,
        marginLeft: Width('4%'),
        backgroundColor: '#D64B12',
    },
    input: {
        width: Width('65%'),
        marginLeft: Width('3%'),
    },
    arrow: {
        height: Width('5%'),
        width: Width('5%'),
        tintColor: '#D64B12'
    },
    categoryContainer: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    addCategory: {
        height:Height('4'),
        width: Width('50'),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#DCDCDC'
    },
    category: {
        backgroundColor: '#6c9f27',
        height: Height('4.5%'),
        marginLeft: Width('4%'),
        marginRight: Width('4%'),
        borderRadius: Width('8%'),
        alignItems: 'center',
        justifyContent: 'center',
    }
});
