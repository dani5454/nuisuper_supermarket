import React from 'react';
import {View,ImageBackground,Text,StyleSheet,TextInput,SafeAreaView,TouchableOpacity} from 'react-native';
import {NavigationActions, StackActions} from 'react-navigation';
import { Width, Height } from '../UtilMethods/UtilsMethods';
import Singlenton from '../UtilMethods/Singlenton';
import firebaseModal from '../firebase/Firebase';
import AppLoading from './../components/Loader';
import Fonts from '../UtilMethods/FontFamily';
import Header from '../components/header';

export default class Login extends React.Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);

        this.state = {
            loader: false,
            email: '',
            password: '',
        }
    }

    signIn = () => {
        const {email,password} = this.state;
        if(email.length > 1 && password.length > 1) {
            this.setState({loader: true});
            firebaseModal.signIn(email,password, (resp) => {
                if(!resp.error) {
                   this.fetchUserProfile();
                } else {
                    this.setState({loader: false});
                    setTimeout(() => {
                        alert(resp.errorMessage);
                    }, 500);
                }
            })
        }
    };

    fetchUserProfile = () => {
        let common = Singlenton.getInstance();
        firebaseModal.fetchMarketProfile((resp) => {
            common.setMarket(resp.data);
            this.setState({loader: false});
            setTimeout(() => {
                const resetAction = StackActions.reset({
                    index: 0,
                    routeName: 'TabNavigator',
                    actions: [NavigationActions.navigate({routeName: 'TabNavigator'})],
                });
                this.props.navigation.dispatch(resetAction)
            }, 500);
        });
    };

    render() {
        return (
            <ImageBackground
                source={require('./../assets/images/login.jpg')}
                style={{flex: 1}}>
                <SafeAreaView style={styles.container}>
                    {AppLoading.renderLoading(this.state.loader)}
                    <Header text={'REGISTRATE'} />
                    <View style={styles.inputContainer}>
                        <View style={styles.inputField}>
                            <TextInput
                                style={[Fonts.gothamRounded_bold,{flex: 1}]}
                                placeholder={'Correo electrónico'}
                                autoCapitalize='none'
                                keyboardType={'email-address'}
                                autoCorrect={false}
                                value={this.state.email}
                                onChangeText={email => this.setState({email})}
                            />
                        </View>
                        <View style={[styles.inputField, {marginTop: Height('3%')}]}>
                            <TextInput
                                style={[Fonts.gothamRounded_bold,{flex: 1}]}
                                placeholder={'Contraseña'}
                                autoCapitalize='none'
                                secureTextEntry={true}
                                value={this.state.password}
                                onChangeText={password => this.setState({password})}
                            />
                        </View>
                        <View style={{flex: 1}}>
                            <TouchableOpacity
                                style={styles.loginBtn}
                                onPress={() => this.signIn()}
                            >
                                <Text style={[Fonts.gothamRounded_bold, {color: '#ffffff'}]}>
                                    INICIAR SESION
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </SafeAreaView>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    inputContainer: {
        flex: 1,
        marginTop: Height('3%')
    },
    inputField: {
        height: Height('4.5%'),
        borderBottomWidth: 2,
        borderBottomColor: '#D64B12',
        marginLeft: Width('4%'),
        marginRight: Width('4%'),
    },
    loginBtn: {
        backgroundColor: '#d64b12',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: Height('3%'),
        borderRadius: Width('6.5%'),
        marginLeft: Width('10%'),
        height: Height('5%'),
        width: Width('40%'),
    }
});
