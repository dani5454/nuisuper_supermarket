import React from 'react';
import {View,Text,TouchableWithoutFeedback,TouchableOpacity,Keyboard,SafeAreaView,StyleSheet} from 'react-native';
import {TextField} from 'react-native-material-textfield';
import SelectionModel from '../components/SelectionModel';
import {Height, Width} from '../UtilMethods/UtilsMethods';
import ActionSheet from 'react-native-actionsheet';
import Fonts from '../UtilMethods/FontFamily';
import Loader from '../components/Loader';
import Back from '../components/Back';
import ImagePicker from 'react-native-image-picker';
import firebaseModal from '../firebase/Firebase';

export default class CreateProduct extends React.Component {

    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Añadir Producto',
            headerTitleStyle: Fonts.gothamRounded_bold,
            headerLeft: () => <Back navigation={navigation} />,
        };
    };

    constructor(props) {
        super(props);

        this.state = {
            loader: false,
            name: '',
            categories: this.props.navigation.state.params.category,
            products: this.props.navigation.state.params.product,
            price: '',
            image: '',
            description: '',
            active: undefined,
            cat: undefined,
            catType: '',
            showModel: false,
            showCategory: false,
        };
    }

    onSelectItem = (index) => {
        if (index === 0) {
            this.showPhotoGallery()
        } else if (index === 1) {
            this.showCameraPicker()
        }
    };

    showPhotoGallery = () => {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,
            },
        };
        ImagePicker.launchImageLibrary(options, ImageResponse => {
            this.capturePhoto(ImageResponse);
        });
    };

    showCameraPicker = () => {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,
            },
        };
        ImagePicker.launchCamera(options, ImageResponse => {
            this.capturePhoto(ImageResponse);
        })
    };

    capturePhoto = (ImageResponse) => {
        if (ImageResponse.didCancel) {
            //console.log('User cancelled photo picker');
        } else if (ImageResponse.error) {
            //console.log('ImagePicker Error: ', ImageResponse.error);
        } else if (ImageResponse.customButton) {
            //console.log('User tapped custom button: ', ImageResponse.customButton);
        } else {
            let source = ImageResponse.uri;
            this.setState({image: source});
            alert('Foto seleccionada con éxito');
        }
    };

    submitActive = (active) => {
        if(active.item === 'True') {
            this.setState({active: true});
            this.onCrossChange();
        } else {
            this.setState({active: false});
            this.onCrossChange();
        }
    };

    onCrossChange = () => {
        this.setState({showModel: false})
    };

    selectCategory = (item) => {
        this.setState({cat: true,catType: item.item});
        this.onCrossPress()
    };

    onCrossPress = () => {
        this.setState({showCategory: false})
    };

    addProduct = () => {
        const {name,price,image,description,catType,active,products} = this.state;
        if(name.length < 1) {
            return
        } else if(price.length < 1) {
            return
        } else if(image.length < 1) {
            return
        } else if(description.length < 1) {
            return
        } else if(catType.length < 1) {
            return
        } else if(active === undefined) {
            return
        } else {
            this.setState({loader: true});
            firebaseModal.uploadImage(image, (resp) => {
                if(!resp.error) {
                    this.addToProductsCart(name,price,image,description,catType,active,products);
                } else {
                    this.setState({loader: false});
                    setTimeout(() => {
                        alert(resp.errorMessage)
                    }, 500);
                }
            }).done();
        }
    };

    addToProductsCart = (name,price,image,description,catType,active,products) => {
        products.map((item, index) => {
            if(item.marketName === catType) {
                let newProduct = [{
                    name: name,
                    price: price,
                    image: image,
                    description: description,
                    category_type: catType,
                    active: active,
                    min_limit: item.products[0].min_limit,
                }];
                products[index].products.push(...newProduct)
            }
        });
        firebaseModal.updateMarketStatus(products, (resp) => {
            if(!resp.error) {
                this.setState({loader: false});
                setTimeout(() => {
                    alert('Producto agregado con éxito');
                }, 500);
            } else {
                this.setState({loader: false});
                setTimeout(() => {
                    alert(resp.errorMessage)
                }, 500);
            }
        });
    };

    render() {
        return (
            <SafeAreaView style={styles.container}>

                {Loader.renderLoading(this.state.loader)}
                <SelectionModel
                    visible={this.state.showModel}
                    showTextInput={false}
                    title={'Active'}
                    data={['True','False']}
                    renderItem={(item) => (
                        <TouchableOpacity
                            onPress={() => this.submitActive(item)}
                            style={{height: Height('5%'), flexDirection: 'row', alignItems: 'center'}}>
                            <Text>{item.item}</Text>
                        </TouchableOpacity>
                    )}
                    onCrossPressed={this.onCrossChange.bind(this)}
                />
                <SelectionModel
                    visible={this.state.showCategory}
                    showTextInput={false}
                    title={'Categoría'}
                    data={this.state.categories}
                    renderItem={(item) => (
                        <TouchableOpacity
                            onPress={() => this.selectCategory(item)}
                            style={{height: Height('5%'), flexDirection: 'row', alignItems: 'center'}}>
                            <Text>{item.item}</Text>
                        </TouchableOpacity>
                    )}
                    onCrossPressed={this.onCrossPress.bind(this)}
                />
                <ActionSheet
                    ref={o => this.ActionSheet = o}
                    title={'Profile Pic'}
                    options={['Seleccionar galería', 'Toma una foto', 'Cancelar']}
                    cancelButtonIndex={2}
                    destructiveButtonIndex={3}
                    onPress={(index) => {
                        this.onSelectItem(index)
                    }}
                />
                <View style={styles.captureImage}>
                    <TouchableWithoutFeedback
                        onPress={() => this.ActionSheet.show()}
                    >
                        <View style={styles.button}>
                            <Text style={[Fonts.gothamRounded_bold, {color: '#FFF'}]}>Agregar Foto</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                <View style={[styles.container, styles.inputContainer]}>
                    <TextField
                        ref={this.name}
                        value={this.state.name}
                        tintColor={'#6c9f27'}
                        lineWidth={2}
                        autoCorrect={false}
                        onChangeText={(name) => this.setState({name})}
                        onSubmitEditing={() => this.refs.price.focus()}
                        label='Product Name'
                    />
                    <TextField
                        ref="price"
                        value={this.state.price}
                        tintColor={'#6c9f27'}
                        keyboardType='phone-pad'
                        lineWidth={2}
                        autoCorrect={false}
                        onChangeText={(price) => this.setState({price})}
                        label='price'
                    />
                    <TextField
                        ref="description"
                        value={this.state.description}
                        tintColor={'#6c9f27'}
                        lineWidth={2}
                        autoCorrect={false}
                        onChangeText={(description) => this.setState({description})}
                        onSubmitEditing={() => Keyboard.dismiss()}
                        label='description'
                    />
                    <TouchableWithoutFeedback
                        onPress={() => this.setState({showModel: true})}
                    >
                        <View style={styles.active}>
                            <View style={{marginBottom: Width('1.5')}}>
                                {this.state.active === undefined ?
                                    <Text style={{color: 'rgba(0, 0, 0, .38)'}}>estado activo</Text> :
                                    <Text style={Fonts.gothamRounded_bold}>
                                        {this.state.active ? 'True' : "False"}
                                    </Text>
                                }
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback
                        onPress={() => this.setState({showCategory: true})}
                    >
                        <View style={[styles.active, {height: Height('8')}]}>
                            <View style={{marginBottom: Width('1.5')}}>
                                {this.state.cat === undefined ?
                                    <Text style={{color: 'rgba(0, 0, 0, .38)'}}>selecciona una categoría</Text> :
                                    <Text style={Fonts.gothamRounded_bold}>
                                        {this.state.catType}
                                    </Text>
                                }
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                <View style={styles.bottomContainer}>
                    <TouchableWithoutFeedback
                        onPress={() => this.addProduct()}
                    >
                        <View style={styles.readyBtn}>
                            <Text style={[Fonts.gothamRounded_bold, {color: '#FFF'}]}>
                                AGREGAR
                            </Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    captureImage: {
        height: Height('8'),
        alignItems: 'center'
    },
    button: {
        height: Height('5'),
        width: Width('30'),
        borderRadius: Width('2'),
        marginTop: Height('1'),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#6c9f27'
    },
    inputContainer: {
        marginLeft: Width('10'),
        marginRight: Width('10'),
        flexDirection: 'column'
    },
    active: {
        borderBottomWidth: 2,
        borderBottomColor: 'rgba(0, 0, 0, .38)',
        height: Height('7'),
        justifyContent: 'flex-end'
    },
    bottomContainer: {
        flex: 1,
        marginTop: Height('10'),
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    readyBtn: {
        height: Height('5'),
        width: Width('85'),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: Width('8'),
        backgroundColor: '#6c9f27'
    }
});
