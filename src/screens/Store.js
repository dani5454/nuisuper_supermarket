import React from 'react';
import {View, Text, SafeAreaView, StyleSheet, TouchableWithoutFeedback, ScrollView, FlatList} from 'react-native';
import { Width, Height } from '../UtilMethods/UtilsMethods';
import { NavigationEvents } from 'react-navigation';
import firebaseModal from '../firebase/Firebase';
import firebase from 'react-native-firebase';
import Fonts from '../UtilMethods/FontFamily';
import Card from '../components/Card';
import Singlenton from '../UtilMethods/Singlenton';

export default class Store extends React.Component {
    common = Singlenton.getInstance();

    constructor(props) {
        super(props);

        this.state = {
            tabNavigator: false,
            orders: [],
            dispatchOrders: [],
            dispatch: false,
        }
    }

    componentDidMount() {
        //TODO => STATUS 'requested', 'accepted', 'dispatched', 'completed', 'rejected'
        this.checkPermission();
    }

    async checkPermission() {
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            this.updateUserToken();
        } else {
            this.requestPermission();
        }
    }

    async requestPermission() {
        try {
            await firebase.messaging().requestPermission();
            // User has authorised
            this.updateUserToken();
        } catch (error) {
            // User has rejected permissions
            console.log('permission rejected');
        }
    }

    updateUserToken = () => {
        firebase.messaging().getToken()
            .then(fcmToken => {
                if (fcmToken) {
                    firebaseModal.updateUserToken(fcmToken, (resp) => {
                        if(!resp.error) {
                            console.log('Token Updated Successfully');
                        }
                    })
                } else {
                    // user doesn't have a device token yet
                }
            });
    };

    firstTab = () => {
        if(this.state.tabNavigator) {
            this.setState({tabNavigator: false})
        }
    };

    secondTab = () => {
        if(!this.state.tabNavigator) {
            this.setState({tabNavigator: true})
        }
    };

    renderOrders = (item, count) => {
        // if(item.userOrder[0].shop === this.common.getMarket().name) {
        let name = item.userDetail.name + ' ' + item.userDetail.lastName;
        let address = item.userDetail.address[0].addressAndNumber;
        let userImage = item.userDetail.socialProfilePic;
        if(item.status === 'requested') {
            return  <Card type={'selection'}
                          index={item.orderId}
                          clientName={name}
                          Direction={address}
                          image={userImage}
                          accept={this.accept.bind(this, item)}
                          reject={this.reject.bind(this, item)}
            />
        }
        // }
    };

    accept = (order) => {
        // let arr = [];
        // const {dispatchOrders} = this.state;
        // firebaseModal.updateOrderStatus(order, (resp) => {
        //     if(!resp.error) {
        //         if(dispatchOrders.length === 0) {
        //             arr.push(order);
        //             this.setState({dispatch: true,dispatchOrders: arr})
        //         } else {
        //             for(let i=0; i<dispatchOrders.length; i++) {
        //                 arr.push(dispatchOrders[i]);
        //             }
        //             arr.push(order);
        //             this.setState({dispatch: true,dispatchOrders: arr})
        //         }
        //     }
        // });
        // this.setState({dispatchOrders: arr})
    };

    reject = (order) => {
        // const {orders} = this.state;
        // orders.map((item, index) => {
        //     if(item.userUID === order.userUID) {
        //         orders.splice(index,1);
        //     }
        // });
        // order.status = 'rejected';
        // firebaseModal.updateOrderStatus(order, (resp) => {
        //     if(!resp.error) {
        //         this.setState({orders})
        //     }
        // })
    };

    renderDispatchOrders = (order, count) => {
        // if(order.userOrder[0].shop === this.common.getMarket().name) {
        let name = order.userDetail.name + ' ' + order.userDetail.lastName;
        let address = order.userDetail.address[0].addressAndNumber;
        let userImage = order.userDetail.socialProfilePic;
        return <Card type={'dispatch'}
                     index={count}
                     clientName={name}
                     Direction={address}
                     image={userImage}
                     dispatch={this.dispatch.bind(this, order)}
        />
        // }
    };

    dispatch = (order) => {
        // const {dispatchOrders} = this.state;
        // dispatchOrders.map((item, index) => {
        //     if(item.userUID === order.userUID) {
        //         dispatchOrders.splice(index,1);
        //     }
        // });
        // order.status = 'dispatched';
        // firebaseModal.updateOrderStatus(order, (resp) => {
        //     if(!resp.error) {
        //         this.setState({dispatchOrders})
        //     }
        // })
    };

    _onWillFocus = (payload) => {
        firebaseModal.fetchOrders((resp) => {
            let acceptedOrder = [];let order = [];
            if(!resp.error) {
                resp.data.filter((item) => {
                    if(item.data().userDetail !== '') {
                        if(item.data().status === 'accepted') {
                            return acceptedOrder.push(item.data());
                        } else if(item.data().status === 'requested') {
                            order.push(item.data());
                        }
                    }
                });
                this.setState({dispatchOrders: acceptedOrder,orders:order})
            } else {
                alert(resp.errorMessage);
            }
        })
    };

    render() {
        const {orders,dispatchOrders} = this.state;
        return (
            <SafeAreaView style={styles.container}>
                <NavigationEvents onWillFocus={payload => this._onWillFocus(payload)} />
               <View style={{justifyContent: 'center', flexDirection: 'row'}}>
                   <TouchableWithoutFeedback
                       onPress={() => this.firstTab()}
                   >
                       <View style={[styles.topLeftNavigator, styles.content, {backgroundColor: !this.state.tabNavigator?'#6c9f27':'#ffffff'}]}>
                           <Text style={Fonts.gothamRounded_bold}>Nueva</Text>
                       </View>
                   </TouchableWithoutFeedback>
                   <TouchableWithoutFeedback
                       onPress={() => this.secondTab()}
                   >
                       <View style={[styles.topRightNavigator, styles.content, {backgroundColor: this.state.tabNavigator?'#6c9f27':'#ffffff'}]}>
                           <Text style={Fonts.gothamRounded_bold}>Aceptada</Text>
                       </View>
                   </TouchableWithoutFeedback>
               </View>
                <View style={styles.container}>
                    <ScrollView>
                        {!this.state.tabNavigator && (
                            <FlatList
                                data={orders}
                                ItemSeparatorComponent={() => <View style={styles.separator} />}
                                renderItem={({ item, index }) => this.renderOrders(item, index)}
                                keyExtractor={(item, index) => `message ${index}`}
                            />
                        )}
                        {this.state.tabNavigator && (
                            <FlatList
                                data={dispatchOrders}
                                ItemSeparatorComponent={() => <View style={styles.separator} />}
                                renderItem={({ item, index }) => this.renderDispatchOrders(item, index)}
                                keyExtractor={(item, index) => `message ${index}`}
                            />
                        )}
                    </ScrollView>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    topLeftNavigator: {
        borderWidth: 1,
        width: Width('30%'),
        height: Height('3%'),
        borderTopLeftRadius: Width('4%'),
        borderBottomLeftRadius: Width('4%'),
    },
    topRightNavigator: {
        borderWidth: 1,
        width: Width('30%'),
        height: Height('3%'),
        borderTopRightRadius: Width('4%'),
        borderBottomRightRadius: Width('4%'),
    },
    content: {
        alignItems: 'center',
        justifyContent: 'center',
    }
});
