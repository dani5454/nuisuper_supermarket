import React from 'react';
import {
    View,
    ImageBackground,
    Text,
    StyleSheet,
    TextInput,
    SafeAreaView,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Dimensions,
} from 'react-native';
import {NavigationActions, StackActions} from 'react-navigation';
import {Width, Height} from '../UtilMethods/UtilsMethods';
import ImagePicker from 'react-native-image-picker';
import ActionSheet from 'react-native-actionsheet';
import Singlenton from '../UtilMethods/Singlenton';
import firebaseModal from '../firebase/Firebase';
import AppLoading from './../components/Loader';
import Fonts from '../UtilMethods/FontFamily';
import Header from '../components/header';

let screenWidth = Dimensions.get('window').width;

export default class Register extends React.Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);

        this.state = {
            loader: false,
            uploadImage: false,
            uploadLogo: false,
            firstName: '',
            lastName: '',
            minPrice: '',
            email: '',
            image: '',
            logo: '',
            address: '',
            password: '',
            confirmPassword: ''
        }
    }

    signUp = () => {
        const {firstName,lastName,email,address,password,image,minPrice,logo,confirmPassword} = this.state;
        if(firstName.length < 1) {
            alert('Por favor ingrese el nombre');
            return;
        }
        if(lastName.length < 1) {
            alert('Por favor ingrese el apellido');
            return;
        }
        if(address.length < 1) {
            alert('Por favor ingrese la dirección');
            return;
        }
        if(image.length < 1) {
            alert('Por favor seleccione imagen');
            return;
        }
        if(logo.length < 1) {
            alert('Por favor seleccione logo');
            return;
        }
        if(minPrice.length < 1) {
            alert('Por favor seleccione precio mínimo');
            return;
        }
        if(password === confirmPassword) {
            this.setState({loader: true});
            firebaseModal.signUp(email, password, (resp) => {
                if (!resp.error) {
                    this.setUpMarketProfile(firstName,lastName,email,address,image,logo,minPrice,resp);
                } else {
                    this.setState({loader: false});
                    setTimeout(() => {
                        alert(resp.errorMessage);
                    }, 500);
                }
            })
        } else {
            alert('La contraseña y la contraseña de confirmación no coinciden');
        }
    };

    setUpMarketProfile = (firstName,lastName,email,address,image,logo,minPrice,currentUser) => {
        let uid = currentUser.response._user.uid;
        firebaseModal.uploadImage(image, (resp) => {
            this.uploadMarketLogo(uid,email,firstName,lastName,'',address, resp.url,logo,minPrice)
        }).done();
    };

    uploadMarketLogo = (uid,email,firstName,lastName,phone,address,image,logo,minPrice) => {
        firebaseModal.uploadImage(logo, (resp) => {
            firebaseModal.setUpUserProfile(uid,email,firstName,lastName,phone,address,resp.url,image,minPrice,(resp) => {
                if(!resp.error) {
                    this.fetchUserProfile(uid, firstName);
                }
            });
        }).done();
    };

    fetchUserProfile = (uid, marketName) => {
        let common = Singlenton.getInstance();
        firebaseModal.fetchMarketProfile((resp) => {
            common.setMarket(resp.data);
            this.initializeMarket(uid, marketName);
        });
    };

    initializeMarket = (uid, marketName) => {
        firebaseModal.initializeMarket(uid, marketName, (resp) => {
            if(!resp.error) {
                this.setState({loader: false});
                setTimeout(() => {
                    this.navigateToHome();
                }, 500);
            }
        });
    };

    navigateToHome = () => {
        const resetAction = StackActions.reset({
            index: 0,
            routeName: 'TabNavigator',
            actions: [NavigationActions.navigate({routeName: 'TabNavigator'})],
        });
        this.props.navigation.dispatch(resetAction)
    };

    uploadImage = () => {
        this.setState({uploadLogo: false, uploadImage: true});
        this.ActionSheet.show();
    };

    uploadLogo = () => {
        this.setState({uploadLogo: true, uploadImage: false});
        this.ActionSheet.show();
    };

    onSelectItem = (index) => {
        if (index === 0) {
            this.showPhotoGallery()
        } else if (index === 1) {
            this.showCameraPicker()
        }
    };

    showPhotoGallery = () => {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,
            },
        };
        ImagePicker.launchImageLibrary(options, ImageResponse => {
            this.capturePhoto(ImageResponse);
        });
    };

    showCameraPicker = () => {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,
            },
        };
        ImagePicker.launchCamera(options, ImageResponse => {
            this.capturePhoto(ImageResponse);
        })
    };

    capturePhoto = (ImageResponse) => {
        if (ImageResponse.didCancel) {
            //console.log('User cancelled photo picker');
        } else if (ImageResponse.error) {
            //console.log('ImagePicker Error: ', ImageResponse.error);
        } else if (ImageResponse.customButton) {
            //console.log('User tapped custom button: ', ImageResponse.customButton);
        } else {
            let source = ImageResponse.uri;
            if(this.state.uploadImage) {
                this.setState({image: source});
            } else {
                this.setState({logo: source});
            }
        }
    };

    render() {
        return (
            <ImageBackground
                source={require('./../assets/images/register.jpg')}
                style={{flex: 1}}>
                <SafeAreaView style={styles.container}>
                    <ActionSheet
                        ref={o => this.ActionSheet = o}
                        title={'Profile Pic'}
                        options={['Seleccionar galería', 'Toma una foto', 'Cancelar']}
                        cancelButtonIndex={2}
                        destructiveButtonIndex={3}
                        onPress={(index) => {
                            this.onSelectItem(index)
                        }}
                    />
                    {AppLoading.renderLoading(this.state.loader)}
                    <Header text={'INICIAR SESION'} />
                    <View style={styles.inputContainer}>
                        <View style={styles.rowContainer}>
                            <View style={[styles.inputField, {width: screenWidth / 2 -20}]}>
                                <TextInput
                                    style={[Fonts.gothamRounded_bold,{flex: 1}]}
                                    placeholder={'Nombre del mercado'}
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    value={this.state.firstName}
                                    onChangeText={firstName => this.setState({firstName})}
                                />
                            </View>
                            <View style={[styles.inputField, {width: screenWidth / 2}]}>
                                <TextInput
                                    style={[Fonts.gothamRounded_bold,{flex: 1}]}
                                    placeholder={'Nombre del dueño'}
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    value={this.state.lastName}
                                    onChangeText={lastName => this.setState({lastName})}
                                />
                            </View>
                        </View>
                        <View style={[styles.inputField, {marginTop: Height('3%')}]}>
                            <TextInput
                                style={[Fonts.gothamRounded_bold,{flex: 1}]}
                                placeholder={'Correo electrónico'}
                                autoCapitalize='none'
                                keyboardType={'email-address'}
                                autoCorrect={false}
                                value={this.state.email}
                                onChangeText={email => this.setState({email})}
                            />
                        </View>
                        <View style={[styles.inputField, {marginTop: Height('3%')}]}>
                            <TextInput
                                style={[Fonts.gothamRounded_bold,{flex: 1}]}
                                placeholder={'Dirección'}
                                autoCapitalize='none'
                                autoCorrect={false}
                                value={this.state.address}
                                onChangeText={address => this.setState({address})}
                            />
                        </View>
                        <View style={[styles.inputField, {marginTop: Height('3%')}]}>
                            <TextInput
                                style={[Fonts.gothamRounded_bold,{flex: 1}]}
                                placeholder={'Precio mínimo'}
                                autoCapitalize='none'
                                autoCorrect={false}
                                value={this.state.minPrice}
                                onChangeText={minPrice => this.setState({minPrice})}
                            />
                        </View>
                        <View style={[styles.inputField, {marginTop: Height('3%')}]}>
                            <TextInput
                                style={[Fonts.gothamRounded_bold,{flex: 1}]}
                                placeholder={'Contraseña'}
                                autoCapitalize='none'
                                secureTextEntry={true}
                                value={this.state.password}
                                onChangeText={password => this.setState({password})}
                            />
                        </View>
                        <View style={[styles.inputField, {marginTop: Height('3%')}]}>
                            <TextInput
                                style={[Fonts.gothamRounded_bold,{flex: 1}]}
                                placeholder={'Confirmar contraseña'}
                                autoCapitalize='none'
                                secureTextEntry={true}
                                value={this.state.confirmPassword}
                                onChangeText={confirmPassword => this.setState({confirmPassword})}
                            />
                        </View>
                        <View style={styles.container}>
                            <View style={{justifyContent: 'space-around',flexDirection: 'row', marginTop: Height('3')}}>
                                <TouchableWithoutFeedback
                                    onPress={() => this.uploadImage()}
                                >
                                    <View style={styles.button}>
                                        <Text style={[Fonts.gothamRounded_bold, {color: '#FFF', fontSize: Width('3')}]}>
                                            SELECCIONAR IMAGEN</Text>
                                    </View>
                                </TouchableWithoutFeedback>
                                <TouchableWithoutFeedback
                                    onPress={() => this.uploadLogo()}
                                >
                                    <View style={styles.button}>
                                        <Text style={[Fonts.gothamRounded_bold, {color: '#FFF', fontSize: Width('3')}]}>
                                            SELECCIONAR LOGO</Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                            <TouchableOpacity
                                onPress={() => this.signUp()}
                                style={styles.registerBtn}>
                                <Text style={[Fonts.gothamRounded_bold, {color: '#ffffff'}]}>
                                    CREAR CUENTA
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </SafeAreaView>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    inputContainer: {
        flex: 1,
        flexDirection: 'column',
        marginTop: Height('3%')
    },
    rowContainer: {
        flexDirection: 'row',
        marginRight: Width('4%'),
        overflow: 'hidden'
    },
    inputField: {
        height: Height('4.5%'),
        borderBottomWidth: 2,
        borderBottomColor: '#D64B12',
        marginLeft: Width('4%'),
        marginRight: Width('4%'),
    },
    registerBtn: {
        backgroundColor: '#d64b12',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: Height('3%'),
        borderRadius: Width('6.5%'),
        marginLeft: Width('10%'),
        height: Height('5%'),
        width: Width('40%'),
    },
    button: {
        width: Width('45'),
        height: Height('4'),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: Width('3'),
        backgroundColor: '#6c9f27'
    }
});
