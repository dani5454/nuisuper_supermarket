import React from 'react';
import {View, Text, TouchableWithoutFeedback, SafeAreaView, StyleSheet, Image} from 'react-native';
import {NavigationActions, StackActions} from 'react-navigation';
import {Height, Width} from '../UtilMethods/UtilsMethods';
import firebaseModal from '../firebase/Firebase';
import Fonts from '../UtilMethods/FontFamily';
import Back from '../components/Back';

export default class Reports extends React.Component {
    static navigationOptions = ({navigation}) => {
        return {
            title: 'REPORTES',
            headerTitleStyle: Fonts.gothamRounded_bold,
            headerLeft: () => <Back navigation={navigation}/>,
            headerRight: () => <Image
                source={require('./../assets/images/logo.png')}
                style={{
                    marginRight: 20,
                    width: Width('10%'),
                    height: Width('10%'),
                }}
            />,
        };
    };

    constructor(props) {
        super(props);

    }

    weeklySales = () => {
        this.props.navigation.navigate('WeeklyReport')
    };

    monthlySales = () => {
        this.props.navigation.navigate('WeeklyReport')
    };

    extraReport = () => {
        //
    };

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.reports}>
                    <Text style={[Fonts.gothamRounded_bold, {fontSize: Width('6')}]}>REPORTES DE</Text>
                    <Text style={[Fonts.gothamRounded_bold, {fontSize: Width('6')}]}>SUPERMERCADO</Text>
                </View>
                <View style={styles.options}>
                    <TouchableWithoutFeedback
                        onPress={() => this.weeklySales()}
                    >
                        <View style={styles.button}>
                            <Text style={[Fonts.gothamRounded_bold, {color: '#FFF'}]}>VENTAS SEMANAL</Text>
                        </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback
                        onPress={() => this.monthlySales()}
                    >
                        <View style={styles.button}>
                            <Text style={[Fonts.gothamRounded_bold, {color: '#FFF'}]}>VENTAS MENSUAL</Text>
                        </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback
                        onPress={() => this.extraReport()}
                    >
                        <View style={styles.button}>
                            <Text style={[Fonts.gothamRounded_bold, {color: '#FFF'}]}>PEPORTES EXTRA</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    reports: {
        marginTop: Height('3'),
        flexDirection: 'column',
        alignItems: 'center'
    },
    options: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    button: {
        width: Width('40'),
        height: Height('4'),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: Width('3'),
        backgroundColor: 'lightgrey'
    }
});
