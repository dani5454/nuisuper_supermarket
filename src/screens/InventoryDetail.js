import React from 'react';
import {View,Text,ScrollView,TouchableWithoutFeedback,SafeAreaView,StyleSheet} from 'react-native';
import {Height,Width} from '../UtilMethods/UtilsMethods';
import Fonts from '../UtilMethods/FontFamily';
import Back from '../components/Back';

export default class InventoryDetail extends React.Component {
    static navigationOptions = ({navigation}) => {
        return {
            headerStyle: {backgroundColor: '#6c9f27'},
            headerTitleStyle: Fonts.gothamRounded_bold,
            headerLeft: () => <Back navigation={navigation}/>,
        };
    };

    constructor(props) {
        super(props);

        this.state = {
            inventoryInfo: this.props.navigation.state.params.inventoryInfo,
            total: 0
        };

    }

    componentDidMount() {
        let total = 0;
        const {inventoryInfo} = this.state;
        inventoryInfo.userOrder.forEach((value, index) =>{
             total += parseInt(value.price) * value.quanity;
        });
        this.setState({total});
    }

    render() {
        const {inventoryInfo,total} = this.state;
        return (
            <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
                <SafeAreaView>
                    <View style={styles.innerContainer}>
                        <View style={[styles.orderId, styles.elevation]}>
                            <Text style={Fonts.gothamRounded_bold}>Orden NUI-{inventoryInfo.userUID.substring(0,8)}</Text>
                        </View>
                        <View style={[styles.orderClient, styles.elevation]}>
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <Text style={Fonts.gothamRounded_bold}>Fecha:   </Text>
                                <Text style={Fonts.gothamRounded_light}>21/09/2019 1:23 PM</Text>
                            </View>
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <Text style={Fonts.gothamRounded_bold}>Cliente: </Text>
                                <Text style={Fonts.gothamRounded_light}>gennevi becker</Text>
                            </View>
                        </View>
                        <View style={styles.priceTag}>
                            <View style={[{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}]}>
                                <Text style={[Fonts.gothamRounded_bold, {fontSize: Width('3')}]}>1x Poutine Canadiense (Especialidades)</Text>
                                <Text style={[Fonts.gothamRounded_bold, {fontSize: Width('3')}]}>{total}</Text>
                            </View>
                            <View style={{marginTop: 8,marginLeft: 3}}>
                            <Text style={[Fonts.gothamRounded_light, {fontSize: Width('3')}]}>Elige tu opcion</Text>
                                <View style={{marginLeft: 10}}>
                                    <Text style={[Fonts.gothamRounded_light, {fontSize: Width('3')}]}>
                                        → Pollo  y bacon ranch (Papas fritas Quebec, con cheese curds y gravy de hongas, pollo
                                        y garlic ranch) + 89.00
                                    </Text>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.itemDescription, styles.elevation]}>
                            <View>
                                <Text style={Fonts.gothamRounded_bold}>Monitoreo de orden</Text>
                            </View>
                                {inventoryInfo.status === 'requested' && (
                                    <View style={{marginLeft: Width('5'), flex: 1, flexDirection: 'column'}}>
                                        <View style={styles.box}>
                                            <Text style={[Fonts.gothamRounded_bold, {color: '#FFF',left: 5, fontSize: 10}]}>Orden Ingresada</Text>
                                            <Text style={[Fonts.gothamRounded_bold, {color: '#FFF',left: 5, fontSize: 10}]}>Hora: Orden Ingresada</Text>
                                        </View>
                                    </View>
                                )}
                                {inventoryInfo.status === 'accepted' && (
                                    <View style={{marginLeft: Width('5'), flex: 1, flexDirection: 'column'}}>
                                        <View>
                                            <Text style={[Fonts.gothamRounded_bold, {color: '#FFF',left: 5, fontSize: 10}]}>Orden Ingresada</Text>
                                            <Text style={[Fonts.gothamRounded_bold, {color: '#FFF',left: 5, fontSize: 10}]}>Hora: Orden Ingresada</Text>
                                        </View>
                                        <View style={[styles.box, {marginTop: Width('4'),width: Width('57')}]}>
                                            <Text style={[Fonts.gothamRounded_bold, {color: '#FFF',left: 5, fontSize: 10}]}>Orden aceptada por restaurante</Text>
                                            <Text style={[Fonts.gothamRounded_bold, {color: '#FFF',left: 5, fontSize: 10}]}>Hora: Orden aceptada por restaurante</Text>
                                        </View>
                                    </View>
                                )}
                                {inventoryInfo.status === 'dispatched' && (
                                    <View style={{marginLeft: Width('5'), flex: 1, flexDirection: 'column'}}>
                                        <View style={styles.box}>
                                            <Text style={[Fonts.gothamRounded_bold, {color: '#FFF',left: 5, fontSize: 10}]}>Orden Ingresada</Text>
                                            <Text style={[Fonts.gothamRounded_bold, {color: '#FFF',left: 5, fontSize: 10}]}>Hora: Orden Ingresada</Text>
                                        </View>
                                        <View style={[styles.box, {marginTop: Width('4'),width: Width('57')}]}>
                                            <Text style={[Fonts.gothamRounded_bold, {color: '#FFF',left: 5, fontSize: 10}]}>Orden aceptada por restaurante</Text>
                                            <Text style={[Fonts.gothamRounded_bold, {color: '#FFF',left: 5, fontSize: 10}]}>Hora: Orden aceptada por restaurante</Text>
                                        </View>
                                        <View style={[styles.box, {marginTop: Width('4'),width: Width('40')}]}>
                                            <Text style={[Fonts.gothamRounded_bold, {color: '#FFF',left: 5, fontSize: 10}]}>Orden despachada</Text>
                                            <Text style={[Fonts.gothamRounded_bold, {color: '#FFF',left: 5, fontSize: 10}]}>Hora: Orden despachada</Text>
                                        </View>
                                    </View>
                                )}
                        </View>
                        <View style={[styles.novice, styles.elevation]}>
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <Text style={Fonts.gothamRounded_bold}>Total de la ordern: </Text>
                                <Text style={Fonts.gothamRounded_light}>{total}</Text>
                            </View>
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <Text style={Fonts.gothamRounded_bold}>Commision Hugo: </Text>
                                <Text style={Fonts.gothamRounded_light}>35.82 (18)%</Text>
                            </View>
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <Text style={Fonts.gothamRounded_bold}>Total a pagar: </Text>
                                <Text style={Fonts.gothamRounded_light}>163.18</Text>
                            </View>
                        </View>
                        <View style={[styles.returnTo, styles.elevation]}>
                            <TouchableWithoutFeedback
                                onPress={() => console.log('Return')}
                            >
                                <View style={styles.button}>
                                    <Text style={[Fonts.gothamRounded_bold, {color: '#FFF'}]}>REGRESAR</Text>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                </SafeAreaView>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
   container: {
       backgroundColor: '#f2efe9'
   },
    innerContainer: {
        marginLeft: Width('3'),
        marginRight: Width('3')
    },
    orderId: {
        height: Height('4'),
        backgroundColor: '#FFFFFF',
        borderBottomLeftRadius: Width('1'),
        borderBottomRightRadius: Width('1'),
        alignItems: 'center',
        justifyContent: 'center',
    },
    elevation: {
        elevation: 4,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
    },
    orderClient: {
        marginTop: Height('2'),
        backgroundColor: '#FFFFFF',
        height: Height('8'),
        borderRadius: Width('1'),
        flexDirection: 'column',
        justifyContent: 'space-around',
        padding: Width('3')
    },
    priceTag: {
        height: Height('14'),
        padding: Width('3'),
        backgroundColor: '#dbb6db',
        marginTop: Height('2'),
    },
    itemDescription: {
        marginTop: Height('2'),
        backgroundColor: '#FFFFFF',
        height: Height('35'),
        flexDirection: 'column',
        padding: Width('5'),
    },
    box: {
        borderRadius: 4,
        backgroundColor: '#4d1e94',
        height: Height('7'),
        width: Width('38'),
        flexDirection: 'column',
        justifyContent: 'center'
    },
    novice: {
        marginTop: Height('2'),
        height: Height('15'),
        backgroundColor: '#FFFFFF',
        flexDirection: 'column',
        justifyContent: 'space-around',
        padding: Width('4')
    },
    returnTo: {
        marginTop: Height('2'),
        height: Height('10'),
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        width: Width('45'),
        height: Height('6'),
        borderRadius: Width('1'),
        backgroundColor: '#839596',
        alignItems: 'center',
        justifyContent: 'center'
    }
});
