import React from 'react';
import {NavigationActions, StackActions} from 'react-navigation';
import Singlenton from '../UtilMethods/Singlenton';
import firebaseModal from '../firebase/Firebase';
import firebase from 'react-native-firebase';
import {View, Image} from 'react-native';

export default class HomeScreen extends React.Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        let common = Singlenton.getInstance();
        if(firebase.auth().currentUser) {
            firebaseModal.fetchMarketProfile((resp) => {
               if(!resp.error) {
                   common.setMarket(resp.data);
               }
            });
            setTimeout(() => {
                const resetAction = StackActions.reset({
                    index: 0,
                    routeName: 'TabNavigator',
                    actions: [NavigationActions.navigate({routeName: 'TabNavigator'})],
                });
                this.props.navigation.dispatch(resetAction)
            }, 5500)
        } else {
            setTimeout(() => {
                this.props.navigation.navigate('Landing');
            }, 5500)
        }
    }


    render() {
        return (
            <View style={{ flex: 1 }}>
                <Image source={require('./../assets/images/splash_gif.gif')}
                       style={{flex: 1, height: null, width: null}}
                       resizeMode={'cover'}
                />
            </View>
        );
    }
}
