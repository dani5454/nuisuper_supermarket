import React from 'react';
import {
    Image,
    SafeAreaView,
    StyleSheet,
    Text,
    TextInput,
    TouchableWithoutFeedback,
    View,
} from 'react-native';
import {Height, Width} from '../UtilMethods/UtilsMethods';
import Singlenton from '../UtilMethods/Singlenton';
import ActionSheet from 'react-native-actionsheet';
import ImagePicker from 'react-native-image-picker';
import firebaseModal from '../firebase/Firebase';
import Fonts from '../UtilMethods/FontFamily';
import Back from '../components/Back';

export default class UpdateMarket extends React.Component {
    common = Singlenton.getInstance();

    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Perfil de Supermercado',
            headerTitleStyle: Fonts.gothamRounded_bold,
            headerLeft: () => <Back navigation={navigation} />,
        };
    };

    constructor(props) {
        super(props);

        this.state = {
            marketName: this.common.getMarket().name,
            address: this.common.getMarket().address,
            pegregal: this.common.getMarket().lastName,
            minimumPrice: this.common.getMarket().minimumPrice.toString(),
            phoneNumber: this.common.getMarket().phone.toString(),
            uploadPhoto: false,
            photo: this.common.getMarket().photo,
            uploadLogo: false,
            logo: this.common.getMarket().logo,
        };
    }

    uploadPhoto = () => {
        this.setState({uploadLogo: false, uploadPhoto: true});
        this.ActionSheet.show();
    };

    uploadLogo = () => {
        this.setState({uploadLogo: true, uploadPhoto: false});
        this.ActionSheet.show();
    };

    onSelectItem = (index) => {
        if (index === 0) {
            this.showPhotoGallery()
        } else if (index === 1) {
            this.showCameraPicker()
        }
    };

    showPhotoGallery = () => {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,
            },
        };
        ImagePicker.launchImageLibrary(options, ImageResponse => {
            this.capturePhoto(ImageResponse);
        });
    };

    showCameraPicker = () => {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,
            },
        };
        ImagePicker.launchCamera(options, ImageResponse => {
            this.capturePhoto(ImageResponse);
        })
    };

    capturePhoto = (ImageResponse) => {
        if (ImageResponse.didCancel) {
            //console.log('User cancelled photo picker');
        } else if (ImageResponse.error) {
            //console.log('ImagePicker Error: ', ImageResponse.error);
        } else if (ImageResponse.customButton) {
            //console.log('User tapped custom button: ', ImageResponse.customButton);
        } else {
            let source = ImageResponse.uri;
            if(this.state.uploadPhoto) {
                this.setState({photo: source});
            } else {
                this.setState({logo: source});
            }
        }
    };

    updateMarket = () => {
        const {marketName,address,pegregal,minimumPrice,phoneNumber,photo,logo} = this.state;
        if(marketName < 1) {
            return true;
        } else if(address.length < 1) {
            return true;
        } else if(pegregal.length < 1) {
            return true;
        } else if(minimumPrice.length < 1) {
            return true;
        } else if(phoneNumber.length < 1) {
            return true;
        } else if(photo.length < 1) {
            return true;
        } else if(logo.length < 1) {
            return true;
        } else {
            let email = this.common.getMarket().email;
            if(photo === this.common.getMarket().photo) {
                this.checkLogo(email,marketName,pegregal,phoneNumber,address,minimumPrice,photo,logo);
            } else {
                firebaseModal.uploadImage(photo,(resp) => {
                    this.updateRecord(email,marketName,pegregal,phoneNumber,address,minimumPrice,resp.url,logo);
                }).done();
            }
        }

    };

    checkLogo = (email,name,pegregal,phone,address,price,photo,logo) => {
        if (logo === this.common.getMarket().logo) {
            this.updateRecord(email,name,pegregal,phone,address,price,photo,logo);
        } else {
            firebaseModal.uploadImage(logo,(resp) => {
                this.updateRecord(email,name,pegregal,phone,address,price,photo,resp.url);
            }).done();
        }
    };

    updateRecord = (email,name,pegregal,phone,address,price,photo,logo) => {
        firebaseModal.updateMarketProfile(email,name,pegregal,phone,address,price,photo,logo, (resp) => {
            if(!resp.error) {
                let market = {
                    email: email,
                    name: name,
                    lastName: pegregal,
                    phone: phone,
                    minimumPrice: price,
                    logo: logo,
                    photo: photo,
                    address: address,
                    customer: false
                };
                this.common.setMarket(market);
                alert('Información actualizada con éxito');
            } else {
                alert(resp.errorMessage);
            }
        });
    };

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <ActionSheet
                    ref={o => this.ActionSheet = o}
                    title={'Profile Pic'}
                    options={['Seleccionar galería', 'Toma una foto', 'Cancelar']}
                    cancelButtonIndex={2}
                    destructiveButtonIndex={3}
                    onPress={(index) => {
                        this.onSelectItem(index)
                    }}
                />
                <View style={styles.photoContainer}>
                    <Image source={{uri: this.state.photo}}
                           style={{height: null, width: null, flex: 1,borderRadius: Width('2')}}
                           resizeMode={'cover'}
                    />
                </View>
                <View style={{alignItems: 'center'}}>
                    <TouchableWithoutFeedback
                        onPress={() => this.uploadPhoto()}
                    >
                        <View style={styles.button}>
                            <Text style={[Fonts.gothamRounded_bold, {color: '#fff'}]}>Subir Foto</Text>
                        </View>
                    </TouchableWithoutFeedback>
                    <View style={styles.logoContainer}>
                        <Image source={{uri: this.state.logo}}
                               style={{height: null, width: null, flex: 1}}
                               resizeMode={'cover'}
                        />
                    </View>
                    <TouchableWithoutFeedback
                        onPress={() => this.uploadLogo()}
                    >
                        <View style={styles.button}>
                            <Text style={[Fonts.gothamRounded_bold, {color: '#fff'}]}>Subir logo</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                <View style={styles.container}>
                    <View style={styles.inputContainer}>
                        <View style={styles.input}>
                            <TextInput
                                style={[Fonts.gothamRounded_bold,{flex: 1}]}
                                placeholder={'market name'}
                                value={this.state.marketName}
                                onChangeText={(marketName) => this.setState({marketName})}
                            />
                        </View>
                        <View style={styles.input}>
                            <TextInput
                                style={[Fonts.gothamRounded_bold,{flex: 1}]}
                                placeholder={'market name'}
                                value={this.state.address}
                                onChangeText={(address) => this.setState({address})}
                            />
                        </View>
                        <View style={styles.input}>
                            <TextInput
                                style={[Fonts.gothamRounded_bold,{flex: 1}]}
                                placeholder={'Col Pedregal'}
                                value={this.state.pegregal}
                                onChangeText={(pegregal) => this.setState({pegregal})}
                            />
                        </View>
                        <View style={styles.input}>
                            <TextInput
                                style={[Fonts.gothamRounded_bold,{flex: 1}]}
                                keyboardType={'numeric'}
                                placeholder={'Enter minimum price'}
                                value={this.state.minimumPrice}
                                onChangeText={(minimumPrice) => this.setState({minimumPrice})}
                            />
                        </View>
                        <View style={styles.input}>
                            <TextInput
                                style={[Fonts.gothamRounded_bold,{flex: 1}]}
                                keyboardType={'numeric'}
                                placeholder={'Phone Number'}
                                value={this.state.phoneNumber}
                                onChangeText={(phoneNumber) => this.setState({phoneNumber})}
                            />
                        </View>
                    </View>
                    <View style={styles.bottomContainer}>
                        <TouchableWithoutFeedback
                            onPress={() => this.updateMarket()}
                        >
                            <View style={styles.readyBtn}>
                                <Text style={[Fonts.gothamRounded_bold, {color: '#6c9f27'}]}>
                                    LISTO
                                </Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    photoContainer: {
        height: Height('10'),
        marginLeft: Width('10'),
        marginRight: Width('10'),
        marginTop: Height('1')
    },
    button: {
        height: Height('5'),
        width: Width('28'),
        borderRadius: Width('2'),
        marginTop: Height('2'),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#6c9f27'
    },
    logoContainer: {
        height: Height('10'),
        width: Width('60'),
        marginTop: Height('2')
    },
    inputContainer: {
        alignItems: 'center'
    },
    input: {
        marginTop: Height('2'),
        width: Width('70'),
        height: Height('4'),
        borderBottomWidth: 2,
        borderColor: '#6c9f27'
    },
    bottomContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    readyBtn: {
        height: Height('5'),
        width: Width('85'),
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        borderRadius: Width('8'),
        borderColor: '#6c9f27'
    }
});
