import React from "react";
import {
    Text,
    View,
    Image,
    StyleSheet,
} from 'react-native';
import Fonts from '../UtilMethods/FontFamily';
import { Width } from '../UtilMethods/UtilsMethods';

const Header = ({
                    text,
                }) => (
    <View style={styles.header}>
        <Image source={require('./../assets/images/logo.png')}
               style={styles.logo}
               resizeMode={'contain'}
        />
        <Text style={[Fonts.gothamRounded_bold, {marginRight: 5}]}>{text}</Text>
    </View>
);

export default Header;

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    logo: {
        width: Width('15%'),
        height: Width('15%'),
        marginLeft: 5
    },
});
