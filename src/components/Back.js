import React from 'react';
import {Image, TouchableOpacity} from 'react-native';
import {Width} from '../UtilMethods/UtilsMethods';


export default class Back extends React.Component {
    render() {
        return (
            <TouchableOpacity
                onPress={() => this.props.navigation.goBack()}
            >
                <Image
                    source={require('./../assets/images/arrowBack.png')}
                    style={{
                        marginLeft: 10,
                        width: Width('7%'),
                        height: Width('7%'),
                    }}
                />
            </TouchableOpacity>

        );
    }
}
