import React from "react";
import {
    Text,
    View,
    Modal,
    TouchableOpacity,
    TextInput,
    Image,
    StyleSheet,
    Dimensions,
    FlatList
} from "react-native";

const { height, width } = Dimensions.get("window");

const SelectionModel = ({
                            visible,
                            onRequestClose,
                            showTextInput,
                            title,
                            data,
                            renderItem,
                            extraData,
                            onChangeText,
                            onSubmitEditing,
                            onCrossPressed
                        }) => (
    <Modal transparent={true} visible={visible} onRequestClose={onRequestClose}>
        <View style={styles.container}>
            <View style={styles.innerContainer}>
                <Text style={styles.titleText}>{title}</Text>

                <TouchableOpacity
                    onPress={onCrossPressed}
                    style={styles.crossButtonStyle}
                >
                    <Image source={require('./../assets/images/cross.png')}
                           style={{height: 15, width: 15}}
                           resizeMode={'cover'}
                    />
                </TouchableOpacity>
                {showTextInput && (
                    <View style={styles.inputContainer}>
                        <TextInput
                            placeholder={'Search'}
                            style={styles.inputStyle}
                            onChangeText={onChangeText}
                            returnKeyType="search"
                            onSubmitEditing={onSubmitEditing}
                            underlineColorAndroid="transparent"
                        />
                    </View>
                )}
                <FlatList
                    data={data}
                    renderItem={renderItem}
                    extraData={extraData}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={() => (
                        <View
                            style={{ height: 0.5, width: "100%", backgroundColor: "#D3D3D3" }}
                        />
                    )}
                />
            </View>
        </View>
    </Modal>
);

export default SelectionModel;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: "rgba(0,0,0,0.5)"
    },
    innerContainer: {
        width: width - 70,
        height: height / 1.9,
        alignSelf: "center",
        borderRadius: 10,
        backgroundColor: "#fff",
        padding: 12
    },
    titleText: {
        fontSize: 15,
        fontWeight: "500",
        textAlign: "center"
    },
    inputContainer: {
        height: 40,
        width: "100%",
        borderWidth: 0.8,
        borderRadius: 5,
        borderColor: "grey",
        justifyContent: "center",
        marginVertical: 10,
        paddingHorizontal: 8
    },
    inputStyle: {
        height: "100%",
        width: "100%"
    },
    crossButtonStyle: {
        position: "absolute",
        right: 13,
        top: 10
    }
});
