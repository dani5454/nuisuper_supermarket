import React from "react";
import {
    Text,
    View,
    ImageBackground,
    TouchableWithoutFeedback,
    StyleSheet,
} from 'react-native';
import { Width, Height } from '../UtilMethods/UtilsMethods';
import Fonts from '../UtilMethods/FontFamily';
import Avatar from './Avatar';

const DeliveryCard = ({
                  type,
                  index,
                  image,
                  orderNo,
                  clientName,
                  Direction,
                  onComplete,
              }) => (
    <ImageBackground
        source={require('./../assets/images/notification.png')}
        style={styles.cardContainer}
        resizeMode={'cover'}
    >
        <View style={styles.container}>
            <View style={{flex: 1, flexDirection: 'row', marginTop: Height('2%')}}>
                <Avatar
                    image={{uri: image}}
                    dim={Width('22%')}
                />
                <View style={{flex: 1,flexDirection: 'column', justifyContent: 'center'}}>
                    <Text style={Fonts.gothamRounded_light}>Orden # NUI-{index}</Text>
                    <Text style={Fonts.gothamRounded_light}>Cliente: {clientName}</Text>
                    <Text style={Fonts.gothamRounded_light}>Direccion: {Direction}</Text>
                    <View style={{flexDirection: 'row', marginTop: Width('3%')}}>
                        {type === 'selection' ?
                            <View style={{flexDirection: 'row'}}>
                                    <View style={[styles.button, {width: Width('50%'),height: Height('4%')}]}>
                                        <Text style={[Fonts.gothamRounded_bold, {color: '#6c9f27'}]}>Orden en Camino</Text>
                                    </View>
                            </View> :
                            <View style={{flexDirection: 'row'}}>
                                <TouchableWithoutFeedback
                                    onPress={() => onComplete()}
                                >
                                    <View style={[styles.button, {width: Width('50%'),height: Height('4%'),backgroundColor: '#6c9f27'}]}>
                                        <Text style={[Fonts.gothamRounded_bold, {color: '#ffffff'}]}>Entregada</Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                        }
                    </View>
                </View>
            </View>
        </View>
    </ImageBackground>
);

export default DeliveryCard;

const styles = StyleSheet.create({
    cardContainer: {
        height: Height('23%'),
        width: Width('100%')
    },
    container: {
        flex: 1,
        marginTop: Height('5%'),
        marginBottom: Height('1.5%'),
        marginLeft: Width('5%'),
        marginRight: Width('5%'),
    },
    button: {
        height: Height('3%'),
        width: Width('23%'),
        borderRadius: Width('1.2%'),
        alignItems: 'center',
        justifyContent: 'center'
    }
});
