import React from 'react';
import {Image,TouchableOpacity,Alert,TouchableWithoutFeedback,SafeAreaView,Text,View,StyleSheet} from 'react-native';
import {StackActions, NavigationActions} from 'react-navigation';
import {Width, Height} from '../UtilMethods/UtilsMethods';
import Singlenton from '../UtilMethods/Singlenton';
import firebaseModal from '../firebase/Firebase';
import Fonts from '../UtilMethods/FontFamily';

export default class Drawer extends React.Component {
    common = Singlenton.getInstance();

    constructor(props) {
        super(props);
    }

    logout = () => {
        this.props.navigation.closeDrawer();
        Alert.alert(
            'cerrar sesión',
            '¿Estás seguro de que quieres cerrar sesión?',
            [
                {
                    text: 'CANCELAR',
                    onPress: () => console.log('cancel'),
                    style: 'destructive',
                },
                {text: 'OK', onPress: () => this.signOut()},
            ],
            {cancelable: false},
        );
    };

    signOut = () => {
        firebaseModal.signOut().done();
        this.common.setMarket(null);
        const resetAction = StackActions.reset({
            index: 0,
            routeName: 'Landing',
            actions: [NavigationActions.navigate({routeName: 'Landing'})],
        });
        this.props.navigation.dispatch(resetAction)
    };

    render() {
        return(
            <SafeAreaView style={{flex: 1, backgroundColor: '#6c9f27'}}>
                <View style={{alignItems: 'center'}}>
                    <Image source={require('./../assets/images/notification_white.png')}
                           style={{width: Width('15%'),height: Width('15%') }}
                    />
                </View>
                <View style={{marginLeft: Width('3%'), marginRight: Width('3%'), flex: 1}}>
                    <TouchableWithoutFeedback
                        onPress={() => this.props.navigation.navigate('UpdateMarket')}
                    >
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                            <View style={{flexDirection: 'column'}}>
                                <Text style={styles.colors}>{this.common.getMarket().name}</Text>
                                <Text style={styles.colors}>{this.common.getMarket().email.toLowerCase()}</Text>
                                <Text style={styles.colors}>{this.common.getMarket().phone}</Text>
                            </View>
                            <View style={{flexDirection: 'column', alignItems: 'center'}}>
                                <View style={styles.profilePhoto}>
                                    <Image source={{uri: this.common.getMarket().photo}}
                                           style={styles.profilePhoto}
                                           resizeMode={'cover'}
                                    />
                                </View>
                                <Text style={styles.colors}>{this.common.getMarket().name}</Text>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                    <View style={{borderWidth: 2, borderColor: 'white'}}/>
                    <View style={{flex: 1}}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('AddCategory')}
                            style={{marginTop: Height('2.4%')}}>
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <View style={{backgroundColor: 'white',width: 30, height: 30, borderRadius: 50}}/>
                                <Text style={[styles.colors,Fonts.gothamRounded_bold, {marginLeft: Width('4%')}]}>AGREGAR CATEGORIAS</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('AddBrand')}
                            style={{marginTop: Height('2.4%')}}>
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <View style={{backgroundColor: 'white',width: 30, height: 30, borderRadius: 50}}/>
                                <Text style={[styles.colors,Fonts.gothamRounded_bold, {marginLeft: Width('4%')}]}>AGREGAR MARCA</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('AddProducts')}
                            style={{marginTop: Height('2.4%')}}>
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <View style={{backgroundColor: 'white',width: 30, height: 30, borderRadius: 50}}/>
                                <Text style={[styles.colors,Fonts.gothamRounded_bold, {marginLeft: Width('4%')}]}>AGREGAR PRODUCTOS</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('ProductStatus')}
                            style={{marginTop: Height('2.4%')}}>
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <View style={{backgroundColor: 'white',width: 30, height: 30, borderRadius: 50}}/>
                                <Text style={[styles.colors,Fonts.gothamRounded_bold, {marginLeft: Width('4%')}]}>INVENTARIO DE SUPER</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('Reports')}
                            style={{marginTop: Height('2.4%')}}>
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <View style={{backgroundColor: 'white',width: 30, height: 30, borderRadius: 50}}/>
                                <Text style={[styles.colors,Fonts.gothamRounded_bold, {marginLeft: Width('4%')}]}>REPORTES</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('Inventory')}
                            style={{marginTop: Height('2.4%')}}>
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <View style={{backgroundColor: 'white',width: 30, height: 30, borderRadius: 50}}/>
                                <Text style={[styles.colors,Fonts.gothamRounded_bold, {marginLeft: Width('4%')}]}>ARCHIVO ORDENES</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{flex: 1,marginTop: Height('7.2%'),backgroundColor: 'lightgrey'}}>
                    <TouchableWithoutFeedback
                        onPress={() => this.logout()}
                    >
                        <View style={styles.logout}>
                            <Image source={require('./../assets/images/logout-512.png')}
                                   style={{height: 20, width: 20,marginLeft: Width('5%')}}
                                   resizeMode={'cover'}
                            />
                            <Text style={[Fonts.gothamRounded_bold, {marginLeft: Width('8%')}]}>
                                cerrar sesión</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    colors: {
        color: 'white'
    },
    profilePhoto: {
        borderRadius: 50, height: 45, width: 45
    },
    logout: {
        height: Height('5'),
        borderBottomWidth: .3,
        flexDirection: 'row',
        alignItems: 'center',
    }
});
